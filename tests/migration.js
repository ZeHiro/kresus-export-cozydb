import 'should';
import * as fs from "fs";
import * as path from 'path';

import { run, main } from '../server';

const DB_PATH = path.join(__dirname, 'db');

let options = {
    dbName: DB_PATH
};

describe('properly run a migration from a populated database', async () => {
    it('should run properly', async () => {
        let output = await run(options);

        output.accesses.length.should.equal(1);
        output.accesses[0].login.should.equal('login');
        output.accesses[0].password.should.equal('sesame');
    });
});

describe('export in CLI mode', () => {
    let outputFile = path.join('/tmp', 'kresus-test-temporary.json');

    it('should properly run and not keep the password', async () => {
        await main(options, outputFile);
        let content = JSON.parse(fs.readFileSync(outputFile, 'utf8'));
        content.accesses.length.should.equal(1);
        content.accesses[0].login.should.equal('login');
        should.equal(content.accesses[0].password, undefined);
    })
});
