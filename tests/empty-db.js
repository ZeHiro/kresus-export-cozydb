import 'should';
import * as fs from "fs";
import * as path from "path";

import { run } from '../server';

const TESTS_DIR = '/tmp/kresus-tests';
const DB_PATH = path.join(TESTS_DIR, 'kresus-export-cozydb-db-path');

// Thanks stackoverflow!
var rmdir = function(dir) {
    var list = fs.readdirSync(dir);
    for(var i = 0; i < list.length; i++) {
        var filename = path.join(dir, list[i]);
        var stat = fs.statSync(filename);
        if (filename == "." || filename == "..") {
            // pass these files
        } else if(stat.isDirectory()) {
            // rmdir recursively
            rmdir(filename);
        } else {
            // rm fiilename
            fs.unlinkSync(filename);
        }
    }
    fs.rmdirSync(dir);
};

describe('properly run a migration from an empty database', async () => {
    before(() => {
        if (fs.existsSync(TESTS_DIR)) {
            rmdir(TESTS_DIR);
        }
        fs.mkdirSync(TESTS_DIR)
    })

    it('should run properly', async () => {
        let options = {
            dbName: DB_PATH
        };
        await run(options);
    });
});
