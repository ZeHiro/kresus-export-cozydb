#!/bin/bash
set -e

yarn run build:server

concurrently -k \
    "./scripts/watch/index.sh" \
    "yarn run -- nodemon --watch ./build/server --watch ./bin/export.js ./bin/export.js -- --config config.ini --output /tmp/output.json"
