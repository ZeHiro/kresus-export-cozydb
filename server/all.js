import Accesses from './models/accesses';
import Accounts from './models/accounts';
import Alerts from './models/alerts';
import Budgets from './models/budgets';
import Categories from './models/categories';
import Settings from './models/settings';
import Transactions from './models/transactions';
import { run as runMigrations } from './models/migrations';

import DefaultSettings from './shared/default-settings';
import { ConfigGhostSettings } from './lib/ghost-settings';
import { makeLogger } from './helpers';

let log = makeLogger('controllers/all');

// Strip away Couchdb/pouchdb metadata.
function cleanMeta(obj) {
    delete obj._id;
    delete obj._rev;
    delete obj.docType;
    return obj;
}

export function cleanData(world) {
    let accessMap = {};
    let nextAccessId = 0;

    world.accesses = world.accesses || [];
    for (let a of world.accesses) {
        accessMap[a.id] = nextAccessId;
        a.id = nextAccessId++;
    }

    let accountMap = {};
    let nextAccountId = 0;
    world.accounts = world.accounts || [];
    for (let a of world.accounts) {
        a.accessId = accessMap[a.accessId];
        accountMap[a.id] = nextAccountId;
        a.id = nextAccountId++;
    }

    let categoryMap = {};
    let nextCatId = 0;
    world.categories = world.categories || [];
    for (let c of world.categories) {
        categoryMap[c.id] = nextCatId;
        c.id = nextCatId++;
    }

    world.budgets = world.budgets || [];
    for (let b of world.budgets) {
        if (typeof categoryMap[b.categoryId] === 'undefined') {
            log.warn(`unexpected category id for a budget: ${b.categoryId}`);
        } else {
            b.categoryId = categoryMap[b.categoryId];
        }

        delete b.id;
    }

    world.operations = world.operations || [];
    for (let o of world.operations) {
        if (typeof o.categoryId !== 'undefined') {
            let cid = o.categoryId;
            if (typeof categoryMap[cid] === 'undefined') {
                log.warn(`unexpected category id for a transaction: ${cid}`);
            } else {
                o.categoryId = categoryMap[cid];
            }
        }

        o.accountId = accountMap[o.accountId];

        // Strip away id.
        delete o.id;

        // Remove attachments, if there are any.
        delete o.attachments;
        delete o.binary;
    }

    world.settings = world.settings || [];
    let settings = [];
    for (let s of world.settings) {
        if (!DefaultSettings.has(s.key)) {
            log.warn(`Not exporting setting "${s.key}", it does not have a default value.`);
            continue;
        }

        if (ConfigGhostSettings.has(s.key) || s.key === 'migration-version') {
            // Don't export ghost settings, since they're computed at runtime.
            continue;
        }

        delete s.id;

        // Properly save the default account id if it exists.
        if (
            s.key === 'default-account-id' &&
            s.value !== DefaultSettings.get('default-account-id')
        ) {
            let accountId = s.value;
            if (typeof accountMap[accountId] === 'undefined') {
                log.warn(`unexpected default account id: ${accountId}`);
                continue;
            } else {
                s.value = accountMap[accountId];
            }
        }

        settings.push(s);
    }
    world.settings = settings;

    world.alerts = world.alerts || [];
    for (let a of world.alerts) {
        a.accountId = accountMap[a.accountId];
        delete a.id;
    }

    return world;
}

export async function getAllData(userId, isExport = false, cleanPassword = true) {
    log.info('Running migrations...');
    await runMigrations();
    log.info('Done.');

    let ret = {};
    ret.accounts = (await Accounts.all(userId)).map(cleanMeta);
    ret.accesses = (await Accesses.all(userId)).map(cleanMeta);

    for (let access of ret.accesses) {
        // Process enabled status only for the /all request.
        if (!isExport) {
            access.enabled = access.isEnabled();
        }

        // Just keep the name and the value of the field.
        access.fields = access.fields.map(({ name, value }) => {
            return { name, value };
        });

        if (cleanPassword) {
            delete access.password;
        }
    }

    ret.categories = (await Categories.all(userId)).map(cleanMeta);
    ret.operations = (await Transactions.all(userId)).map(cleanMeta);
    ret.settings = (isExport
        ? await Settings.allWithoutGhost(userId)
        : await Settings.all(userId)
    ).map(cleanMeta);

    if (isExport) {
        ret.budgets = (await Budgets.all(userId)).map(cleanMeta);
    }

    // Return alerts only if there is an email recipient.
    let emailRecipient = ret.settings.find(s => s.key === 'email-recipient');
    if (emailRecipient && emailRecipient.value !== DefaultSettings.get('email-recipient')) {
        ret.alerts = (await Alerts.all(userId)).map(cleanMeta);
    } else {
        ret.alerts = [];
    }

    return cleanData(ret);
}
